function hdfsdu() {
  hdfs dfs -du -h / | grep -v "Unable to load native-hadoop library"
  echo
  hdfs dfs -du -h /"$1" | grep -v "Unable to load native-hadoop library"
}

function khdfsdu() {
  kubectl -n hdfs exec -it "$1" -- hdfs dfs -du -h /
  echo
  kubectl -n hdfs exec -it "$1" -- hdfs dfs -du -h /"$2"
}

function hdfs_usage_report() {
  echo "latest run: $(date)\n"
  hdfs dfsadmin -report -dead | tail -n +2
  hdfs dfsadmin -report | grep 'Name:\|DFS Used%' | tail -n +2 | paste -d " " - - | sort
}
