# path of this script's directory
DIR=$(dirname "${(%):-%x}")
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi

for f in "$DIR"/*.sh; do
    [[ -e "$f" ]] || break  # handle the case of no *.sh files
    source "$f"
done
