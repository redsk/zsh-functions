function gitlisttags() {
  git --no-pager tag --sort=v:refname
}

function gitlatesttag() {
  res=$(git --no-pager tag --sort=v:refname | tail -n 1)
  echo "$res"
  echo -n "$res" | pbcopy
}

function gitloglast() {
  res=$(git --no-pager log --pretty=format:"%s" -1)
  echo "$res"
  echo -n "$res" | pbcopy
}

function gittagauto() {
  res="gittag $(gitlatesttag) \"$(gitloglast)\""
  echo "$res"
  echo -n "$res" | pbcopy
}

function gittag() {
  TAG=$1
  MESSAGE=${@:2}

  test -z "$TAG" && echo "usage: gittag <tag> <message>" && return 1
  test -z "$MESSAGE" && echo "usage: gittag <tag> <message>" && return 1

  echo -e "git tag -a $TAG -m \"$MESSAGE\""
  echo -e "git push origin $TAG"

  git tag -a "$TAG" -m "$MESSAGE"
  git push origin "$TAG"
}

function gitbranch() {
  git rev-parse --abbrev-ref HEAD
}
