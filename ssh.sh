function sshy() {
  HOST=$1
  PORT=$2

  test -z "$HOST" && echo "usage: sshy <host> <port>" && return 1
  test -z "$PORT" && echo "usage: sshy <host> <port>" && return 1

  while
    ssh -o "ServerAliveInterval=10" -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" "$HOST" -p "$PORT"
    RES=$?
    if (($RES == 0)); then
      break
    fi
    sleep 1
  do :; done
}
