function jsonpretty() {
  python -m json.tool < "$1" | pygmentize -l json
}
