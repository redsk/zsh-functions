function rep() {
  repe 3 "$@"
}

function repe() {
  while :; do
    output=$("${@:2}")
    clear
    echo -n "$output"
    sleep "$1"
  done
}

function repesize() {
  while :; do
    output=$("${@:2}")
    new_size=$(echo "$output" | gawk '{ if (length($0) > max) max = length($0) } END { print NR " " max }' -)
    current_size="$(tput lines) $(tput cols)"
    if [ "$new_size" != "$current_size" ]; then
      read -r rows cols <<<"$new_size"
      resize "$rows" "$cols"
    fi
    clear
    echo -n "$output"
    sleep "$1"
  done
}

function repesizeonce() {
  output=$("${@:2}")
  resize "$(echo "$output" | expand | gsed 's/\x1b\[[0-9;]*[a-zA-Z]//g' | gawk '{ if (length($0) > max) max = length($0) } END { print NR " " max }' -)"
  while :; do
    clear
    echo -n "$output"
    sleep "$1"
    output=$("${@:2}")
  done
}

# rows, columns
function resize() {
  printf "\e[8;$1;$2t"
}
