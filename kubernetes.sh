function kstat() {
  kubectl get pods "${@:1}"
  kubectl get svc "${@:1}"
}

function kstats() {
  for NAMESPACE in "${@:1}"; do
    echo "############## $NAMESPACE"
    kstat -n "$NAMESPACE"
    echo
  done
}

function kubecontext() {
  kubectx && sudo pkill telepresence && telepresence quit && telepresence connect
}