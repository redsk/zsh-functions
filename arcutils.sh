function arcmount() {
  if [ -z "$1" ]; then
    echo "Usage: arcmount <PATH> [BASE_MOUNT_POINT] [BASE_STORE_POINT]"
    echo "  PATH               : The arc path to be mounted."
    echo "  BASE_MOUNT_POINT   : The base mount point (default: ~/arcwww)."
    echo "  BASE_STORE_POINT   : The base store point (default: ~/arc)."
    return 1
  fi

  local MOUNT_PATH=$1
  local BASE_MOUNT_POINT=${2:-~/arcwww}
  local BASE_STORE_POINT=${3:-~/arc}

  local PATH_LEAF=$(basename "$MOUNT_PATH")

  arc mount --mount="$BASE_MOUNT_POINT"/"$PATH_LEAF" --store="$BASE_STORE_POINT"/"$PATH_LEAF" --path-filter="$MOUNT_PATH" --fetch-trees
}

function arcmountall() {
  local MOUNT_POINTS_FILE=${1:-~/.arcmountpoints}

  # Check if the file exists and is readable
  if [ ! -f "$MOUNT_POINTS_FILE" ] || [ ! -r "$MOUNT_POINTS_FILE" ]; then
    echo "Usage: arcmountall [MOUNT_POINTS_FILE]"
    echo "  MOUNT_POINTS_FILE : A text file containing paths to be mounted, one per line (default: ~/.arcmountpoints)."
    return 1
  fi

  while IFS= read -r PATH_LINE; do
    # Skip empty lines and lines that start with '#'
    if [ -n "$PATH_LINE" ] && [[ "$PATH_LINE" != \#* ]]; then
      echo "Mounting PATH_LINE: $PATH_LINE"
      arcmount "$PATH_LINE"
    fi
  done < "$MOUNT_POINTS_FILE"
}

function arcumountall() {
  local MOUNT_POINTS_FILE=${1:-~/.arcmountpoints}
  local BASE_MOUNT_POINT=${2:-~/arcwww}
  local BASE_STORE_POINT=${3:-~/arc}

  # Check if the file exists and is readable
  if [ ! -f "$MOUNT_POINTS_FILE" ] || [ ! -r "$MOUNT_POINTS_FILE" ]; then
    echo "Usage: arcumountall [MOUNT_POINTS_FILE]"
    echo "  MOUNT_POINTS_FILE : A text file containing paths to be unmounted, one per line (default: ~/.arcmountpoints)."
    return 1
  fi

  while IFS= read -r PATH_LINE; do
    # Skip empty lines and lines that start with '#'
    if [ -n "$PATH_LINE" ] && [[ "$PATH_LINE" != \#* ]]; then
      local PATH_LEAF=$(basename "$PATH_LINE")
      local MOUNT_PATH="$BASE_MOUNT_POINT"/"$PATH_LEAF"

      echo "Unmounting MOUNT_PATH: $MOUNT_PATH"

      # Attempt to unmount using arc
      if arc umount "$MOUNT_PATH"; then
        echo "Successfully unmounted $MOUNT_PATH with arc."
      else
        # If arc umount fails, check for 'resource busy' and attempt with diskutil
        echo "arc umount failed for $MOUNT_PATH. Attempting diskutil unmount force..."
        if diskutil unmount force "$MOUNT_PATH"; then
          echo "Successfully unmounted $MOUNT_PATH with diskutil."
        else
          echo "Failed to unmount $MOUNT_PATH with both arc and diskutil. Please unmount manually."
        fi
      fi
    fi
  done < "$MOUNT_POINTS_FILE"
}

function arctagpush() {
  if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Usage: arctagpush <PROJECT> <TAG>"
    echo "  PROJECT: The project name."
    echo "  TAG: The arc tag name in semver."
    return 1
  fi

  local PROJECT=$1
  local TAG=$2

  arc tag "$TAG" && arc push "$TAG":releases/"$PROJECT"/"$TAG"
}

function arclastrel() {
  if [ -z "$1" ]; then
    echo "Usage: arclastrel <PROJECT NAME>"
    echo "  PROJECT NAME: The arc release project name."
    return 1
  fi

  arcrels "$1" | tail -n 1
}

function arcrels() {
  if [ -z "$1" ]; then
    echo "Usage: arcrels <PROJECT NAME>"
    echo "  PROJECT NAME: The arc release project name."
    return 1
  fi

  local PROJECT=$1

  arc fetch releases/"$PROJECT"/
  arc branch -a | grep "releases/$PROJECT/" | awk -F'/' '{print $NF}' | grep -E '^[0-9]+\.[0-9]+\.[0-9]+$' | sort -V
}