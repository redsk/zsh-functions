# sane control-u
bindkey "^U" backward-kill-line

# useful aliases
alias grepp="grep * -H -n -r -i -e "
alias doco=docker-compose
alias coden="code --new-window"
