function iterm() {
    osascript - "$@" <<EOF
on run argv
tell application "iTerm"
    set new_term to (create window with default profile)
    tell new_term
        tell the current session
	    # write text (item 1 of argv) without newline
            repeat with arg in argv
	       write text " " without newline
               write text arg without newline
            end repeat
	    write text ""
        end tell
    end tell
end tell
end run
EOF
}

# Java
# use as "jdk 11"
jdksilent() {
  version=$1
  unset JAVA_HOME
  export JAVA_HOME=$(/usr/libexec/java_home -v"$version");
}
jdk() {
  jdksilent $1
  java -version
}
