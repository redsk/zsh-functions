# Creates credentials to access AWS with Multi Factor Authentication. Uses
# - 1password:
#   - https://1password.com
#   - https://support.1password.com/command-line-getting-started/
# - AWS CLI
# - oathtool
# - jq
# - dasel https://github.com/TomWright/dasel
#
# Before the first run:
#   cp ~/.aws/credentials ~/.aws/credentials.backup
#   cp ~/.aws/credentials ~/.aws/credentials.mta
#   edit ~/.aws/credentials.mta to use 'mta' instead of default for your username
#   cp ~/.aws/credentials.mta ~/.aws/credentials
#
# For the first run only:
#   export OP_SECRET_KEY=<your secret key>
#   eval $(op account add --address <1password address> --email <youremail> --signin)
#
# usage: updateAwsCredentials '<your aws account name in 1password>' 'your AWS MFA ARN'
function updateAwsCredentials() {
  ONEPASS_ITEM=$1
  AWS_MFA_ARN=$2

  eval $(op signin --account my)
  OTPCODE=$(op item get "$ONEPASS_ITEM" --otp)
  aws sts get-session-token --duration-seconds 129600 --serial-number "$AWS_MFA_ARN" --token-code "$OTPCODE" --output json --profile mta > ~/.aws/temp-cred.json
  cat ~/.aws/temp-cred.json | jq '.["default"]["aws_access_key_id"] = .Credentials.AccessKeyId | .["default"]["aws_secret_access_key"] = .Credentials.SecretAccessKey | .["default"]["aws_session_token"] = .Credentials.SessionToken | del(.Credentials)' | dasel -r json -w toml | sed 's/"//g' > ~/.aws/temp-cred.toml
  cat ~/.aws/temp-cred.toml ~/.aws/credentials.mta > ~/.aws/credentials
  echo -e "New credentials expire: $(jq '.Credentials.Expiration' ~/.aws/temp-cred.json)"
  rm ~/.aws/temp-cred.toml ~/.aws/temp-cred.json
}
