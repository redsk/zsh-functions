#!/usr/bin/env zsh

zmodload zsh/datetime

function update-zsh-functions() {
    echo "Updating zsh-functions..."

    # path of this script's directory
    DIR=$(dirname ${(%):-%x})
    (cd "$DIR" && git pull --rebase --stat origin master)

    printf "Updated. source your ~/.zshrc:\nsource ~/.zshrc\n"
}

function _current_epoch() {
  echo $(( $EPOCHSECONDS / 60 / 60 / 24 ))
}

function _update_zsh-functions_update() {
  echo "LAST_EPOCH=$(_current_epoch)" >! ~/.zsh-functions-update
}

function _upgrade_zsh-functions() {
  update-zsh-functions
  # update the zsh functions file
  _update_zsh-functions_update
}

epoch_target=$UPDATE_ZSH_FUNCTIONS_DAYS
if [[ -z "$epoch_target" ]]; then
  # Default to old behavior
  epoch_target=13
fi

# Cancel upgrade if git is unavailable on the system
whence git >/dev/null || return 0

if [ -f ~/.zsh-functions-update ]; then
    . ~/.zsh-functions-update

    if [[ -z "$LAST_EPOCH" ]]; then
        _update_zsh-functions_update && return 0
    fi

    epoch_diff=$(($(_current_epoch) - $LAST_EPOCH))
    if [ $epoch_diff -gt $epoch_target ]; then
        echo "[Nico's zsh functions] Would you like to update? [Y/n]: \c"
        read line
        if [[ "$line" == Y* ]] || [[ "$line" == y* ]] || [ -z "$line" ]; then
            _upgrade_zsh-functions
        else
            _update_zsh-functions_update
        fi
    fi
    else

    # create the zsh file
    _update_zsh-functions_update
fi
